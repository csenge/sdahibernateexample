package edu.sda.hibernateTutorial.service;

import edu.sda.hibernateTutorial.model.User;
import edu.sda.hibernateTutorial.util.HibernateUtil;
import org.hibernate.classic.Session;

import java.util.List;

public class UserService {

    public void createUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(user);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public void updateUser(User user) {
        Session session1 = HibernateUtil.getSessionFactory().openSession();
        session1.beginTransaction();

        session1.update(user);
        session1.getTransaction().commit();
        HibernateUtil.shutdown();

    }

    public void saveOrUpdateUser(User user) {
        Session session1 = HibernateUtil.getSessionFactory().openSession();
        session1.beginTransaction();

        session1.saveOrUpdate(user);

        session1.getTransaction().commit();
        HibernateUtil.shutdown();

    }

    public void deleteUser(User user) {
        Session session3 = HibernateUtil.getSessionFactory().openSession();
        session3.beginTransaction();

        session3.delete(user);

        session3.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public void deleteById(Integer id) {
        Session session3 = HibernateUtil.getSessionFactory().openSession();
        session3.beginTransaction();

        User result = (User) session3.load(User.class, id);
        session3.delete(result);

        session3.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public List<User> getAllUsers() {
        Session session2 = HibernateUtil.getSessionFactory().openSession();
        session2.beginTransaction();

        List<User> result = session2.createQuery("from User").list();

        for (User u : result) {
            System.out.println(u.toString());
        }

        session2.getTransaction().commit();
        HibernateUtil.shutdown();

        return result;
    }

    public User getUserByID(Integer id) {

        Session session2 = HibernateUtil.getSessionFactory().openSession();
        session2.beginTransaction();

        User result = (User) session2.load(User.class, id);

        session2.getTransaction().commit();
        HibernateUtil.shutdown();

        return result;
    }
}
