package edu.sda.hibernateTutorial;

import edu.sda.hibernateTutorial.model.User;
import edu.sda.hibernateTutorial.service.UserService;
import edu.sda.hibernateTutorial.util.HibernateUtil;
import org.hibernate.classic.Session;

import java.util.List;

public class Runner {

    public static void main111(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        // Add new user
        User user = new User();
        user.setName("user");

        session.save(user);

        session.getTransaction().commit();
        HibernateUtil.shutdown();


        //Update user
        Session session1 = HibernateUtil.getSessionFactory().openSession();
        session1.beginTransaction();
        user.setName("Csenge");

        session1.update(user);
        session1.getTransaction().commit();
        HibernateUtil.shutdown();

        //Read all users
        Session session2 = HibernateUtil.getSessionFactory().openSession();
        session2.beginTransaction();

        List<User> result = session2.createQuery("from User").list();

        for (User u : result) {
            System.out.println(u.toString());
        }

        session2.getTransaction().commit();
        HibernateUtil.shutdown();

        //delete user
        Session session3 = HibernateUtil.getSessionFactory().openSession();
        session3.beginTransaction();

        session3.delete(user);

        session3.getTransaction().commit();
        HibernateUtil.shutdown();


        System.out.println("Reading the list after deletion");
        //Verify that user is deleted
        Session session4 = HibernateUtil.getSessionFactory().openSession();
        session4.beginTransaction();

        List<User> result1 = session2.createQuery("from User").list();

        System.out.println("Size of user list after deletion: " + result1.size());

        session4.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public static void main2222(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        // Add new user
        User user = new User();
        user.setName("user");

        session.save(user);

        //Update user
        user.setName("Csenge");

        List<User> result = session.createQuery("from User").list();

        for (User u : result) {
            System.out.println(u.toString());
        }

        //delete user
        session.delete(user);

        System.out.println("Reading the list after deletion");
        //Verify that user is deleted

        List<User> result1 = session.createQuery("from User").list();

        System.out.println("Size of user list after deletion: " + result1.size());

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public static void mainFrumos(String[] args) {

        UserService userService = new UserService();

        userService.createUser(new User("Elena"));

        userService.getAllUsers();

        User firstUser = userService.getUserByID(1);

        firstUser.setName("Silvia");

        userService.updateUser(firstUser);

        userService.deleteById(1);

        userService.getAllUsers();
    }
}
